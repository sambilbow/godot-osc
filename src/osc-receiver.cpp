/**************************************************************************/
/*  osc-receiver.cpp                                                      */
/**************************************************************************/
/*                         This file is part of:                          */
/*                          godot-osc extension                           */
/*                        https://godotengine.org                         */
/**************************************************************************/
/* Copyright (c) 2014-present Godot Engine contributors (see AUTHORS.md). */
/* Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.                  */
/*                                                                        */
/* Permission is hereby granted, free of charge, to any person obtaining  */
/* a copy of this software and associated documentation files (the        */
/* "Software"), to deal in the Software without restriction, including    */
/* without limitation the rights to use, copy, modify, merge, publish,    */
/* distribute, sublicense, and/or sell copies of the Software, and to     */
/* permit persons to whom the Software is furnished to do so, subject to  */
/* the following conditions:                                              */
/*                                                                        */
/* The above copyright notice and this permission notice shall be         */
/* included in all copies or substantial portions of the Software.        */
/*                                                                        */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY   */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                 */
/**************************************************************************/

#include "osc-receiver.hpp"

#include "lo-address.hpp"
#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <string>

using namespace godot;

namespace gdext::osc {

void OSCReceiver::set_port(int port) {
  port_ = port;
  server_.reset();
  server_ = std::make_unique<LoServer>(std::to_string(port_).c_str());
}

int OSCReceiver::get_port() const {
  return port_;
}

void OSCReceiver::_bind_methods() {
  // properties
  ClassDB::bind_method(D_METHOD("get_port"), &OSCReceiver::get_port);
  ClassDB::bind_method(D_METHOD("set_port", "p_port"), &OSCReceiver::set_port);
  ClassDB::add_property("OSCReceiver", PropertyInfo(Variant::INT, "port"), "set_port", "get_port");
  ClassDB::bind_method(D_METHOD("pop_messages"), &OSCReceiver::pop_messages);
}

Array OSCReceiver::pop_messages() {
  if (server_) {
    return server_->pop_messages();
  } else {
    UtilityFunctions::printerr("OSCReceiver: called pop message with a not started OSC server! ");
    return Array();
  }
}

void OSCReceiver::_exit_tree() {
   server_.reset();
}

} // namespace gdext::osc
