/**************************************************************************/
/*  osc-sender.cpp                                                        */
/**************************************************************************/
/*                         This file is part of:                          */
/*                          godot-osc extension                           */
/*                        https://godotengine.org                         */
/**************************************************************************/
/* Copyright (c) 2014-present Godot Engine contributors (see AUTHORS.md). */
/* Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.                  */
/*                                                                        */
/* Permission is hereby granted, free of charge, to any person obtaining  */
/* a copy of this software and associated documentation files (the        */
/* "Software"), to deal in the Software without restriction, including    */
/* without limitation the rights to use, copy, modify, merge, publish,    */
/* distribute, sublicense, and/or sell copies of the Software, and to     */
/* permit persons to whom the Software is furnished to do so, subject to  */
/* the following conditions:                                              */
/*                                                                        */
/* The above copyright notice and this permission notice shall be         */
/* included in all copies or substantial portions of the Software.        */
/*                                                                        */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY   */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                 */
/**************************************************************************/

#include "osc-sender.hpp"

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/variant/array.hpp>
#include <godot_cpp/variant/packed_byte_array.hpp>
#include <godot_cpp/variant/packed_color_array.hpp>
#include <godot_cpp/variant/packed_float32_array.hpp>
#include <godot_cpp/variant/packed_float64_array.hpp>
#include <godot_cpp/variant/packed_int32_array.hpp>
#include <godot_cpp/variant/packed_int64_array.hpp>
#include <godot_cpp/variant/packed_string_array.hpp>
#include <godot_cpp/variant/packed_vector2_array.hpp>
#include <godot_cpp/variant/packed_vector3_array.hpp>

#include "lo-address.hpp"

using namespace godot;

namespace gdext::osc {

void OSCSender::set_port(int port) {
  port_ = port;
  lo_addr_.reset();
  lo_addr_ = std::make_unique<LoAddress>(host_.utf8().get_data(), godot::String::num(port_).utf8().get_data());
  if (!lo_addr_) { // handle error
    port_ = 0;
    UtilityFunctions::printerr("cannot set OSC port");
  }
}

int OSCSender::get_port() const {
  return port_;
}

void OSCSender::set_host(const String &host) {
  host_ = host;
  lo_addr_.reset();
  lo_addr_ = std::make_unique<LoAddress>(host_.utf8().get_data(), godot::String::num(port_).utf8().get_data());
  if (!lo_addr_) { // handle error
    port_ = 0;
    UtilityFunctions::printerr("cannot initiate OSC desctination address. Note that a valid (and available) port is required.");
  }
}

String OSCSender::get_host() const {
	return host_;
}


void OSCSender::send(godot::String key, Array values) {
  if (!lo_addr_) {
    lo_addr_ = std::make_unique<LoAddress>(host_.utf8().get_data(), godot::String::num(port_).utf8().get_data());
  }
  if (!lo_addr_->addr()) {
    UtilityFunctions::printerr("OSC message not sent. Are port and host initialized ?");
    return;
  }
  
  // initialisation of the message to fill
  lo_message msg = lo_message_new();

  // iterate over the array add elements to the OSC message
  for (int i = 0; i < values.size(); i++) {
    Variant val = values[i];
    auto vtype = val.get_type();
    if (vtype == Variant::Type::INT) {
      int value = val;
      lo_message_add_int64(msg, value);
    } else if (vtype == Variant::Type::BOOL) {
      bool value = val;
      if(value)
        lo_message_add_true(msg);
      else
        lo_message_add_false(msg);
    } else if (vtype == Variant::Type::FLOAT) {
      float value = val;
      lo_message_add_float(msg, value);
    } else if (vtype == Variant::Type::STRING) {
      String value = val;
      lo_message_add_string(msg, value.utf8().get_data());
    } else if (vtype == Variant::Type::STRING_NAME) {
      String value = val;
      lo_message_add_string(msg, value.utf8().get_data());
    } else if (vtype == Variant::Type::VECTOR2) {
      Vector2 value = val;
      lo_message_add_float(msg, value.x);
      lo_message_add_float(msg, value.y);
    } else if (vtype == Variant::Type::RECT2) {
      Rect2 value = val;
      lo_message_add_float(msg, value.position.x);
      lo_message_add_float(msg, value.position.y);
      lo_message_add_float(msg, value.size.x);
      lo_message_add_float(msg, value.size.y);
    } else if (vtype == Variant::Type::VECTOR3) {
      Vector3 value = val;
      lo_message_add_float(msg, value.x);
      lo_message_add_float(msg, value.y);
      lo_message_add_float(msg, value.z);
    } else if (vtype == Variant::Type::PLANE) {
      Plane value = val;
      lo_message_add_float(msg, value.normal.x);
      lo_message_add_float(msg, value.normal.y);
      lo_message_add_float(msg, value.normal.z);
      lo_message_add_float(msg, value.d);
    } else if (vtype == Variant::Type::AABB) {
      AABB value = val;
      lo_message_add_float(msg, value.position.x);
      lo_message_add_float(msg, value.position.y);
      lo_message_add_float(msg, value.position.z);
      lo_message_add_float(msg, value.size.x);
      lo_message_add_float(msg, value.size.y);
      lo_message_add_float(msg, value.size.z);
    } else if (vtype == Variant::Type::BASIS) {
      Basis value = val;
      lo_message_add_float(msg, value.rows[0].x);
      lo_message_add_float(msg, value.rows[0].y);
      lo_message_add_float(msg, value.rows[0].z);
      lo_message_add_float(msg, value.rows[1].x);
      lo_message_add_float(msg, value.rows[1].y);
      lo_message_add_float(msg, value.rows[1].z);
      lo_message_add_float(msg, value.rows[2].x);
      lo_message_add_float(msg, value.rows[2].y);
      lo_message_add_float(msg, value.rows[2].z);
    } else if (vtype == Variant::Type::VECTOR4) {
      Vector4 value = val;
      lo_message_add_float(msg, value.x);
      lo_message_add_float(msg, value.y);
      lo_message_add_float(msg, value.z);
      lo_message_add_float(msg, value.w);
    } else if (vtype == Variant::Type::TRANSFORM2D) {
      Transform2D value = val;
      lo_message_add_float(msg, value.columns[0].x);
      lo_message_add_float(msg, value.columns[0].y);
      lo_message_add_float(msg, value.columns[1].x);
      lo_message_add_float(msg, value.columns[1].y);
      lo_message_add_float(msg, value.columns[2].x);
      lo_message_add_float(msg, value.columns[2].y);
    } else if (vtype == Variant::Type::TRANSFORM3D) {
      Transform3D value = val;
      lo_message_add_float(msg, value.basis.rows[0].x);
      lo_message_add_float(msg, value.basis.rows[0].y);
      lo_message_add_float(msg, value.basis.rows[0].z);
      lo_message_add_float(msg, value.basis.rows[1].x);
      lo_message_add_float(msg, value.basis.rows[1].y);
      lo_message_add_float(msg, value.basis.rows[1].z);
      lo_message_add_float(msg, value.basis.rows[2].x);
      lo_message_add_float(msg, value.basis.rows[2].y);
      lo_message_add_float(msg, value.basis.rows[2].z);
      lo_message_add_float(msg, value.origin.x);
      lo_message_add_float(msg, value.origin.y);
      lo_message_add_float(msg, value.origin.z);
    } else if (vtype == Variant::Type::QUATERNION) {
      Quaternion value = val;
      lo_message_add_float(msg, value.x);
      lo_message_add_float(msg, value.y);
      lo_message_add_float(msg, value.z);
      lo_message_add_float(msg, value.w);
    } else if (vtype == Variant::Type::PROJECTION) {
      Projection value = val;
      lo_message_add_float(msg, value.columns[0].x);
      lo_message_add_float(msg, value.columns[0].y);
      lo_message_add_float(msg, value.columns[0].z);
      lo_message_add_float(msg, value.columns[0].w);
      lo_message_add_float(msg, value.columns[1].x);
      lo_message_add_float(msg, value.columns[1].y);
      lo_message_add_float(msg, value.columns[1].z);
      lo_message_add_float(msg, value.columns[1].w);
      lo_message_add_float(msg, value.columns[2].x);
      lo_message_add_float(msg, value.columns[2].y);
      lo_message_add_float(msg, value.columns[2].z);
      lo_message_add_float(msg, value.columns[2].w);
      lo_message_add_float(msg, value.columns[3].x);
      lo_message_add_float(msg, value.columns[3].y);
      lo_message_add_float(msg, value.columns[3].z);
      lo_message_add_float(msg, value.columns[3].w);
    } else if (vtype == Variant::Type::COLOR) {
      Color value = val;
      lo_message_add_float(msg, value.r);
      lo_message_add_float(msg, value.g);
      lo_message_add_float(msg, value.b);
      lo_message_add_float(msg, value.a);
    } else if (vtype == Variant::Type::NODE_PATH) {
      String value = val;
      lo_message_add_string(msg, value.utf8().get_data());
    } else if (vtype == Variant::Type::RID) {
      RID value = val;
      lo_message_add_int64(msg, value.get_id());
    } else if (vtype == Variant::Type::PACKED_BYTE_ARRAY) {
      PackedByteArray array = val;
      auto blob = lo_blob_new(array.size(), array.ptr());
      lo_message_add_blob(msg, blob);
      lo_blob_free(blob);
    } else if (vtype == Variant::Type::PACKED_INT32_ARRAY) {
      PackedInt32Array packed_array = val;
      for (auto it = packed_array.begin(); it != packed_array.end(); ++it) {
        lo_message_add_int32(msg, *it);
      }
    } else if (vtype == Variant::Type::PACKED_INT64_ARRAY) {
      PackedInt64Array packed_array = val;
      for (auto it = packed_array.begin(); it != packed_array.end(); ++it) {
        lo_message_add_int64(msg, *it);
      }
    } else if (vtype == Variant::Type::PACKED_FLOAT32_ARRAY) {
      PackedInt64Array packed_array = val;
      for (auto it = packed_array.begin(); it != packed_array.end(); ++it) {
        lo_message_add_float(msg, *it);
      }
    } else if (vtype == Variant::Type::PACKED_FLOAT64_ARRAY) {
      PackedInt64Array packed_array = val;
      for (auto it = packed_array.begin(); it != packed_array.end(); ++it) {
        lo_message_add_double(msg, *it);
      }
    } else if (vtype == Variant::Type::PACKED_STRING_ARRAY) {
      PackedStringArray packed_array = val;
      for (auto it = packed_array.begin(); it != packed_array.end(); ++it) {
        lo_message_add_string(msg, (*it).utf8().get_data());
      }
    } else if (vtype == Variant::Type::PACKED_VECTOR2_ARRAY) {
      PackedVector2Array packed_array = val;
      for (auto it = packed_array.begin(); it != packed_array.end(); ++it) {
        lo_message_add_float(msg, (*it).x);
        lo_message_add_float(msg, (*it).y);
      }
    } else if (vtype == Variant::Type::PACKED_VECTOR3_ARRAY) {
      PackedVector3Array packed_array = val;
      for (auto it = packed_array.begin(); it != packed_array.end(); ++it) {
        lo_message_add_float(msg, (*it).x);
        lo_message_add_float(msg, (*it).y);
        lo_message_add_float(msg, (*it).z);
      }
    } else if (vtype == Variant::Type::PACKED_COLOR_ARRAY) {
      PackedColorArray packed_array = val;
      for (auto it = packed_array.begin(); it != packed_array.end(); ++it) {
        lo_message_add_float(msg, (*it).r);
        lo_message_add_float(msg, (*it).g);
        lo_message_add_float(msg, (*it).b);
        lo_message_add_float(msg, (*it).a);
      }
    } else {
      UtilityFunctions::printerr("OSCSender: unable to add this kind of data! " + Variant::get_type_name(vtype));
    }
  }
  
  lo_send_message(lo_addr_->addr(), key.utf8().get_data(), msg);// "sfsff", "one", 0.12345678f,
          // "three", -0.00000023001f, 1.0);
  lo_message_free(msg);
}

void OSCSender::_bind_methods() {
  // properties
  ClassDB::bind_method(D_METHOD("get_port"), &OSCSender::get_port);
  ClassDB::bind_method(D_METHOD("set_port", "p_port"), &OSCSender::set_port);
  ClassDB::add_property("OSCSender", PropertyInfo(Variant::INT, "port"), "set_port", "get_port");
  ClassDB::bind_method(D_METHOD("get_host"), &OSCSender::get_host);
  ClassDB::bind_method(D_METHOD("set_host", "p_host"), &OSCSender::set_host);
  ClassDB::add_property("OSCSender", PropertyInfo(Variant::STRING, "host"), "set_host", "get_host");

  // method
  ClassDB::bind_method(D_METHOD("send", "key", "value"),  &OSCSender::send);
}

void OSCSender::_exit_tree() {
  lo_addr_.reset();
}

} // namespace gdext::osc
