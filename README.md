# GDExtension OSC

GDExtension OSC is a Godot Extension (think like plug-in) that enables reception and emission of [OSC](http://opensoundcontrol.org/introduction-osc?) messages in [Godot game engine](https://godotengine.org/).

The code will compile into a dynamic library, easy to load and use in Godot projects. It has been tested with Godot version 4.1.

# Install

Binary downloads and install instructions for Linux x86_64 can be found [here](https://gitlab.com/nicobou/godot-osc/-/releases). Otherwise, instructions for building from sources are below.

# Send OSC messages from Godot

This extension comes with the `OSCSender` node. It provides a method for sending: `send("key", [VariantObject1, VariantObject2, ...])`. Destination `Host` and `Port` can be configured using node properties.

The `send` method supports the sending of value with the following variant types: `INT`, `BOOL`, `FLOAT`, `STRING`, `STRING_NAME`, `VECTOR2`, `VECTOR3`, `PLANE`, `AABB`, `BASIS`, `VECTOR4`, `TRANSFORM2D`, `TRANSFORM3D`, `QUATERNION`, `PROJECTION`, `COLOR`, `NODE_PATH`, `RID`, `PACKED_BYTE_ARRAY`, `PACKED_INT32_ARRAY`, `PACKED_INT64_ARRAY`, `PACKED_FLOAT32_ARRAY`, `PACKED_FLOAT64_ARRAY`, `PACKED_STRING_ARRAY`, `PACKED_VECTOR2_ARRAY`, `PACKED_VECTOR3_ARRAY` and `PACKED_COLOR_ARRAY`.

Here follows the example from the `demo-projet`, where OSC sending is performed a each frame, using the `_process` method of an `OSCSender` node. 
```gdscript
# This script is attached to an "OSCSender" node
extends OSCSender

# Following variable internal values will be sent through 
# OSC at each call to "_process"
var cube : Node3D # a MeshInstance3D in the scene.
var packed_array : PackedVector3Array # for computing values to send

func _ready():
	# get a reference to the cube
	cube = get_node("/root/root/cube")
	# create a new PackedVector3Array
	packed_array = PackedVector3Array()
	# Add some Vector3s to the array
	packed_array.push_back(Vector3(1, 2, 3))
	packed_array.push_back(Vector3(4, 5, 6))


func _process(delta):
	# cube rotation in order to get variation in cube rotation
	cube.rotate_y(delta * 1)
	# change value position in the packed_array
	packed_array[0] = packed_array[0].inverse()
	# send cube name (string) and rotation (3 float values)
	send("/root/cube", [cube.name, cube.rotation])
	# send the packed array content (6 float values)
	send("/root/vectarray", [packed_array])
```

Once the project is running, messages can be received at the configured destination host (localhost) and port (7770). Here, using the `oscdump` command:
```bash
$ oscdump 7770
e86172e4.49eafee6 /root/cube sfff "cube" -0.000000 1.516667 0.000000
e86172e4.49eb52c9 /root/vectarray ffffff 1.000000 0.500000 0.333333 4.000000 5.000000 6.000000
e86172e4.4e2a1b5c /root/cube sfff "cube" -0.000000 1.529303 0.000000
e86172e4.4e2a6f3f /root/vectarray ffffff 1.000000 2.000000 3.000000 4.000000 5.000000 6.000000
e86172e4.526bc621 /root/cube sfff "cube" -0.000000 1.545931 0.000000
e86172e4.526c2acb /root/vectarray ffffff 1.000000 0.500000 0.333333 4.000000 5.000000 6.000000
```

# Receiving OSC messages in Godot

This extension comes with the `OSCReceiver` node. It provides a method for receiving messages: `pop_messages()`. `Port` can be configured using the node property. pop_messages returns an array of newly received messages. Each message is also an array, with the OSC path in the first position, and an array of values in the second position.

OSCreceiver supports the following OSC types: `INT32`, `FLOAT`, `STRING`, `BLOB`, `INT64`, `DOUBLE`, `SYMBOL`, `CHAR`, `MIDI`, `TRUE`, `FALSE` and `NIL`.

Here follows the example from the `demo-projet`, where `pop_messages()` is called from the `_process` method of an `OSCReceiver` node. 
```gdscript
# This script is attached to an "OSCReceiver" node
extends OSCReceiver

# cube and env are nodes in the scene
var cube : MeshInstance3D
var env : WorldEnvironment

func _ready():
	# get reference to the cube and env nodes
	cube = get_node("/root/root/cube")
	env = get_node("/root/root/WorldEnvironment") 

func _process(delta):
	# get the messages received since the last call to pop_messages()
	var messages = pop_messages()
	# iterate over the received message
	for msg in messages:
		var key = msg[0] # a string with the OSC path
		var values = msg[1] # an array with the OSC values
		if key == "/cube/rotate":
			# apply rotation with expected values "(float, float, float, float)"
			cube.rotate((delta * Vector3(values[0], values[1], values[2])).normalized(), values[3])
		elif key == "/cube/col":
			# change cube RGBA color, with expected values (float, float, float, float)
			cube.get_surface_override_material(0).albedo_color = Color(values[0], values[1], values[2], values[3])
		elif key == "/cube/visible":
			# change cube visibility with a bool value
			cube.visible = values[0]
		elif key == "/bg/col":
			# change horizon RGBA color, with expected values (float, float, float, float)
			env.environment.sky.sky_material.sky_horizon_color = Color(values[0], values[1], values[2], values[3])
		else:
			# print unhandled keys
			print(key + "?")
```

Once the project is running, messages can be sent to the configured destination host (localhost) and port (7000). Here, using the `oscsend` command:
```bash
oscsend localhost 7000 /cube/rotate ffff 1.0 0 0.3 1
oscsend localhost 7000 /cube/col ffff 1 0 0 1
oscsend localhost 7000 /cube/visible F # visible set to false
oscsend localhost 7000 /cube/visible T # visible set to true
oscsend localhost 7000 /bg/col ffff 0 1 0 1
```

# Build from sources

Several dependencies needs to be installed: `git scons g++ cmake`.

On Ubuntu 22.04, this can be installed with the following command:
```
sudo apt install git scons g++ cmake
```

Then the godot-osc extension can be built as follows:
```
# get the sources
git clone https://gitlab.com/nicobou/godot-osc.git
cd godot-osc
git submodule update --init --recursive

# build the OSC library (liblo) as a static library
mkdir liblo-build
cd liblo-build
cmake ../liblo/cmake -DWITH_STATIC=ON -DWITH_TOOLS=OFF -DWITH_TESTS=OFF -DWITH_EXAMPLES=OFF -DWITH_CPP_TESTS=OFF
make

# build le godot-osc extension
cd ..
scons target=template_release # or 'target=template_debug' for debug mode 
```

In order to add the extension to a Godot project, copy the binary extention, located in the `bin` folder, and the `osc.gdextension` file to the Godot project folder.

## Free, open source and community-driven

Like [Godot](https://github.com/godotengine/godot), this OSC extension is completely free and open source under the very permissive [MIT license](https://godotengine.org/license). No strings attached, no royalties, nothing.

# Thanks

The `godot-osc` repository is a complete rewrite inspired from the [gdosc](https://github.com/MadMcCrow/gdosc) repository. Contributors of the initial gdosc are:

- Perard-Gayot Noé [MadMcCrow]
- François Zajéga [frankiezafe](https://gitlab.com/frankiezafe/)
- Michal Seta - [djiamnot](https://gitlab.com/djiamnot/)
- Bastiaan Olij - [mux213@gmail.com](mux213@gmail.com) - special thanks for dgnative c++ template!

